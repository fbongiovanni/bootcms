﻿
using FluentNHibernate.Mapping;

namespace BootCms.Data
{
    public abstract class Entity<T> : ClassMap<T> where T : class
    {
        public Entity() { }
    }
}
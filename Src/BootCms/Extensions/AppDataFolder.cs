﻿using System;
using System.IO;
using System.Data.SqlServerCe;
using System.Linq;
using System.Web;

namespace BootCms.Extensions
{
    public static class AppDataFolder
    {
        internal static readonly string ConnectionString = "Data Source=|DataDirectory|{0}/{1};Persist Security Info=False;";

        /// <summary>
        /// Path to App_Data folder
        /// </summary>
        /// <returns>The path to app_data folder</returns>
        public static string AppDataPath()
        {
            return AppDomain.CurrentDomain.GetData("DataDirectory").ToString();    
        }

        /// <summary>
        /// Path to App_Data folder
        /// </summary>
        /// <returns>The path to app_data folder</returns>
        public static string PrivatAppDataRootPath()
        {
            return AppDataPath().CombineWithSeparator(Domain());
        }

        private static string CombineWithSeparator(this string first, string second)
        {
            return string.Concat(first, Path.DirectorySeparatorChar, second);
        }

        /// <summary>
        /// Create's a SqlCe database.
        /// </summary>
        /// <param name="sqlCeEngine"></param>
        /// <param name="databasename">The name of the database</param>
        /// <returns>The connectionstring</returns>
        public static string CreateConnectionString(this SqlCeEngine sqlCeEngine, string databasename)
        {
            sqlCeEngine.LocalConnectionString =
                string.Format(ConnectionString, Domain(), AddSdfExtension(databasename));

            CreateHostFolder();

            if (!Exist(AddSdfExtension(databasename)))
                sqlCeEngine.CreateDatabase();

            return sqlCeEngine.LocalConnectionString;
        }

        /// <summary>
        /// Get domain name. The base name of a domain for e.g bootcms.com. Treats www.bootcms.com and bootcms.com as same domain.
        /// but all other subdomain as their own domain. 
        /// </summary>
        /// <returns>Current domain name</returns>
        public static string Domain()
        {
           return HttpContext.Current.Request.Url
                           .GetLeftPart(UriPartial.Authority)
                           .Replace("http://", "")
                           .Replace("www.","");
        }

        /// <summary>
        /// Creates a folder based on the domains name.
        /// </summary>
        private static void CreateHostFolder()
        {
            var folder = PrivatAppDataRootPath();
  
            if (!Directory.EnumerateDirectories(AppDataPath()).Any() || !Directory.Exists(folder))
                Directory.CreateDirectory(folder);
        }

        /// <summary>
        /// Add extension .sfd to a filename if not exist
        /// </summary>
        /// <param name="name">The name to add .sdf of</param>
        /// <returns>A string with .sdf attached</returns>
        private static string AddSdfExtension(string name)
        {
            return !name.EndsWith(".sdf") ? string.Format("{0}.sdf", name) : name;
        }

        /// <summary>
        /// Check if a database exits.
        /// </summary>
        /// <param name="fileName">Filename to check</param>
        /// <returns>True if exist</returns>
        private static bool Exist(this string fileName)
        {
            return File.Exists(string.Format("{0}{3}{1}{3}{2}", AppDataPath(), Domain(), fileName, Path.DirectorySeparatorChar));
        }
    }
}
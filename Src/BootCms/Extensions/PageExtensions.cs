﻿using System.Globalization;
using BootCms;
using BootCms.Data.DataManager;
using BootCms.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

 public static class PageExtensions
{
    /// <summary>
    /// The current url
    /// </summary>
    /// <param name="page">Page p</param>
    /// <returns>The current <see cref="Page"/></returns>
    public static string CurrentUrl(this Page page)
    {
        return HttpContext.Current.Request.RawUrl.ToString(CultureInfo.InvariantCulture).ToLower();
    }

     /// <summary>
     /// The current <see>
     ///         <cref>Page</cref>
     ///     </see>
     /// </summary>
     public static Page CurrentPage(this Page page)
    {
        return BootStarter.SessionFactory.OpenSession().QueryOver<Page>().Where(p => p.Url == new Page().CurrentUrl()).List().First();
    }
}


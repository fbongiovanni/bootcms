﻿using BootCms.Data.DataManager;
using BootCms.Models;
using BootCms.ViewModels;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BootCms.Controllers
{
    /// <summary>
    /// Base Controller
    /// </summary>
    public abstract class BaseController : Controller, IBaseController
    {
        public dynamic Model { get; set; }

        protected BaseController()
        {
            Model = new PageViewModel(BootStarter.SessionFactory.OpenSession());
        }
    }
}
﻿using BootCms.Cache;
using BootCms.Data;
using BootCms.Models;
using NHibernate.Mapping.ByCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BootCms.Modules.Models
{
    public class Content : IEntity
    {
        public virtual new Int32 Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Html { get; set; }
        public virtual Int32 PageId { get; set; }
        public virtual new Region Region { get; set; }
        public virtual bool Active { get; set; }
        public virtual bool CreateLink { get; set; }
        public virtual bool UseTitle { get; set; }

        public Content() : base() { }
    }

    public class ContentMap : Entity<Content>
    {
        public ContentMap() : base()
        {
            Id(x => x.Id)
              .Column("Id")
              .GeneratedBy.Assigned()
              .CustomType<Int32>();
            Map(p => p.Title);
            Map(p => p.Html);
            Map(p => p.PageId);
            Map(p => p.Region);
            Map(p => p.Active);
            Map(p => p.CreateLink);
            Map(p => p.UseTitle);
        }
    } 
}